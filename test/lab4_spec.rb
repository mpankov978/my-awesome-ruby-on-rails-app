require 'rspec'
require '../lib/lab4/lab4_1'
require '../lib/lab4/lab4_2'
require '../lib/lab4/lab4_3'

RSpec.describe 'lab4' do
  context 'lab4_1' do
    before { File.write(ARTIST_LIST_PATH, "Will Smith\nBruce Willis\nHue Grant\n") }
    it '#index' do
      expect { index }.to output("Will Smith\nBruce Willis\nHue Grant\n").to_stdout
    end

    it '#find' do
      expect { find(0) }.to output("Will Smith\n").to_stdout
    end

    it '#find_should throw TypeError_id' do
      expect { find("0") }.to raise_error(TypeError)
    end

    it '#where' do
      expect(where("Bruce Willis")).to eq(1)
    end

    it '#where_should throw TypeError_pattern' do
      expect { where(1152) }.to raise_error(TypeError)
    end

    it '#update' do
      update(1, "Jason Wong")
      expect { index }.to output("Will Smith\nJason Wong\nHue Grant\n").to_stdout
    end

    it '#update_should throw TypeError_id' do
      expect { update("1", "Jason Wong") }.to raise_error(TypeError)
    end

    it '#update_should throw TypeError_text' do
      expect { update(1, 2) }.to raise_error(TypeError)
    end

    it '#delete' do
      delete(2)
      expect { index }.to output("Will Smith\nBruce Willis\n").to_stdout
    end

    it '#delete_should throw TypeError_id' do
      expect { delete("2") }.to raise_error(TypeError)
    end
  end

  context "lab4_2" do
    before do
      File.write("D:/Универ/лабы/3к 5сем/Web-программирование/my-awesome-ruby-on-rails-app/test/results.txt", "", mode: "w")
    end

    it "#show_names with all ages" do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("23", "32", "19", "60", "28", "35", "48", "55", "39", "24")
      expect { show_names }.to output("Все данные из файла считаны:\nГромов Роберт 23\nСеливанова Милана 32\nЕгоров Роман 19\nСтепанов Александр 60\nАбрамов Дмитрий 28\nФилиппов Мирон 35\nСмирнов Александр 48\nМитрофанова Амина 55\nПанова Ольга 39\nПавлов Тимур 24\n").to_stdout
    end

    it "#show_names with some ages and -1" do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("23", "60", "-1")
      expect { show_names }.to output("Получена команда -1:\nГромов Роберт 23\nСтепанов Александр 60\n").to_stdout
    end

    it "#show_names with -1 command" do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("-1")
      expect { show_names }.to output("Получена команда -1:\n").to_stdout
    end

    after do
      File.delete("D:/Универ/лабы/3к 5сем/Web-программирование/my-awesome-ruby-on-rails-app/test/results.txt")
    end
  end

  context "lab4_3" do
    before do
      File.write('D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab4\balance.txt', "200", mode: "w")
    end

    it '#atm_logic deposit' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("d\n", -10, 20, "q\n")
      expect { atm_logic }.to output("Ваш баланс: 200 вечнозеленых\nВведите сумму: \nОшибка! Вы не можете ввести отрицательную сумму. Попробуйте еще раз.\nТеперь у вас 220 вечнозеленых\nВыход из программы\n").to_stdout
    end

    it '#atm_logic withdraw' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("w\n", -10, 1000, 100, "q\n")
      expect { atm_logic }.to output("Ваш баланс: 200 вечнозеленых\nВведите сумму: \nНевозможно вывести данную сумму. Попробуйте еще раз.\nНевозможно вывести данную сумму. Попробуйте еще раз.\nТеперь у вас 100 вечнозеленых\nВыход из программы\n").to_stdout
    end

    it '#atm_logic balance' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("b\n", "q\n")
      expect { atm_logic }.to output("Ваш баланс: 200 вечнозеленых\nВаш баланс: 200 вечнозеленых\nВыход из программы\n").to_stdout
    end

    it '#atm_logic all cases' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("d\n", -50, 20, "w\n", 230, 20, "b\n", "q\n")
      expect { atm_logic }.to output("Ваш баланс: 200 вечнозеленых\nВведите сумму: \nОшибка! Вы не можете ввести отрицательную сумму. Попробуйте еще раз.\nТеперь у вас 220 вечнозеленых\nВведите сумму: \nНевозможно вывести данную сумму. Попробуйте еще раз.\nТеперь у вас 200 вечнозеленых\nВаш баланс: 200 вечнозеленых\nВыход из программы\n").to_stdout
    end

  end
end
