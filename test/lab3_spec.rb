require 'rspec'
require_relative '../lib/lab3/lab3.rb'

RSpec.describe "Main" do
  it "#script1_1" do
    expect(script1(10, "Macs")).to eq(16)
  end
  it "#script1_2" do
    expect(script1(10, "ma")).to eq("am")
  end
  it "#script1_should throw TypeError_number" do
    expect { script1("20", "ma") }.to raise_error(TypeError)
  end
  it "#script1_should throw TypeError_name" do
    expect { script1(20, 20) }.to raise_error(TypeError)
  end
  it "#script2" do
    allow_any_instance_of(Kernel).to receive(:gets).and_return(2, "Пикачу", "Желтый", "Бублозавр", "Синий")
    #color перед name скорее всего из-за лексикографического порядка(алфавитного)
    expect(script2).to eq([{ "color" => "Желтый", "name" => "Пикачу" }, { "color" => "Синий", "name" => "Бублозавр" }])
  end

end