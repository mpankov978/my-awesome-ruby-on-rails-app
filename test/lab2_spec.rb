require 'rspec'
require_relative '../lib/lab2.rb'

describe "print greeting method" do
  it "should print for age >18" do
    expect(STDOUT).to receive(:puts).with("Привет, Максим Паньков. Самое время заняться делом!")
    print_greeting("Максим", "Паньков", 19)
  end
  it "should print for age <18" do
    expect(STDOUT).to receive(:puts).with("Привет, Максим Паньков. Тебе меньше 18 лет, но начать учиться программировать никогда не рано")
    print_greeting("Максим", "Паньков", 17)
  end
  it "should throw TypeError" do
    expect { print_greeting("Максим", "Паньков", "not an Integer") }.to raise_error(TypeError)
  end
end

describe "foobar func" do
  it "should return 10" do
    expect(foobar(20, 10)).to eq(10)
  end
  it "should return 20" do
    expect(foobar(10, 10)).to eq(20)
  end
  it "should throw TypeError" do
    expect { foobar("20", 10) }.to raise_error(TypeError)
  end
end

