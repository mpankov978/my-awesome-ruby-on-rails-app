require 'rspec'
require '../lib/lab5/lab5_1'
require '../lib/lab5/lab5_2'

RSpec.describe 'lab5' do
  context "lab5_1" do
    before do
      File.write('D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab5\balance.txt', "200", mode: "w")
    end

    subject { CashMachine.new }

    it '#deposit' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("d\n", -10, 20, "q\n")
      expect(subject.init)
      expect { subject.show_balance }.to output("Ваш баланс: 220 вечнозеленых\n").to_stdout
    end

    it '#withdraw' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("w\n", -10, 1000, 100, "q\n")
      expect(subject.init)
      expect { subject.show_balance }.to output("Ваш баланс: 100 вечнозеленых\n").to_stdout
    end

    it '#balance' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("b\n", "q\n")
      expect(subject.init)
      expect { subject.show_balance }.to output("Ваш баланс: 200 вечнозеленых\n").to_stdout
    end

    it '#all cases' do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("d\n", -10, 20, "w\n", -10, 100, "q\n")
      expect(subject.init)
      expect { subject.show_balance }.to output("Ваш баланс: 120 вечнозеленых\n").to_stdout
    end

  end

  context "lab5_2" do

    context "Posts" do
      subject { PostsController.new }
      it '#index' do
        allow_any_instance_of(PostsController).to receive(:gets).and_return("My first post. Hello world!", "My second post. Bye world!")
        expect(subject.create)
        expect(subject.create)
        expect { subject.index }.to output("0. My first post. Hello world!\n1. My second post. Bye world!\n").to_stdout
      end

      it '#show' do
        allow_any_instance_of(PostsController).to receive(:gets).and_return("My first post. Hello world!", "0")
        expect(subject.create)
        expect { subject.show }.to output("Input index of the post to show:\n0. My first post. Hello world!\n").to_stdout
      end

      it '#create' do
        allow_any_instance_of(PostsController).to receive(:gets).and_return("My first post. Hello world!")
        expect { subject.create }.to output("Input text of your new beautiful post:\n0. My first post. Hello world!\n").to_stdout
      end

      it '#update' do
        allow_any_instance_of(PostsController).to receive(:gets).and_return("My first post. Hello world!", "My second post. Bye world!", "1", "My updated second post. Hello again!")
        expect(subject.create)
        expect(subject.create)
        expect(subject.update)
        expect { subject.index }.to output("0. My first post. Hello world!\n1. My updated second post. Hello again!\n").to_stdout
      end

      it '#destroy' do
        allow_any_instance_of(PostsController).to receive(:gets).and_return("My first post. Hello world!", "My second post. Bye world!", "1")
        expect(subject.create)
        expect(subject.create)
        expect(subject.destroy)
        expect { subject.index }.to output("0. My first post. Hello world!\n").to_stdout
      end
    end

    context "Comments" do
      subject { CommentController.new }

      it '#index' do
        allow_any_instance_of(CommentController).to receive(:gets).and_return("My first comment. Hello world!", "My second comment. Bye world!")
        expect(subject.create)
        expect(subject.create)
        expect { subject.index }.to output("0. My first comment. Hello world!\n1. My second comment. Bye world!\n").to_stdout
      end

      it '#show' do
        allow_any_instance_of(CommentController).to receive(:gets).and_return("My first comment. Hello world!", "0")
        expect(subject.create)
        expect { subject.show }.to output("Input index of the comment to show:\n0. My first comment. Hello world!\n").to_stdout
      end

      it '#create' do
        allow_any_instance_of(CommentController).to receive(:gets).and_return("My first comment. Hello world!")
        expect { subject.create }.to output("Input text of your new beautiful comment:\n0. My first comment. Hello world!\n").to_stdout
      end

      it '#update' do
        allow_any_instance_of(CommentController).to receive(:gets).and_return("My first comment. Hello world!", "My second comment. Bye world!", "1", "My updated second comment. Hello again!")
        expect(subject.create)
        expect(subject.create)
        expect(subject.update)
        expect { subject.index }.to output("0. My first comment. Hello world!\n1. My updated second comment. Hello again!\n").to_stdout
      end

      it '#destroy' do
        allow_any_instance_of(CommentController).to receive(:gets).and_return("My first comment. Hello world!", "My second comment. Bye world!", "1")
        expect(subject.create)
        expect(subject.create)
        expect(subject.destroy)
        expect { subject.index }.to output("0. My first comment. Hello world!\n").to_stdout
      end
    end
  end
end