# Банкомат
class CashMachine

  PATH_BALANCE = 'D:/Универ/лабы/3к 5сем/Web-программирование/my-awesome-ruby-on-rails-app/lib/lab6/balance.txt'.freeze
  DEFAULT_BALANCE = 100

  # Инициализатор класса
  def initialize
    if File.exist?(PATH_BALANCE)
      @balance = File.read(PATH_BALANCE).to_i
      raise TypeError, 'Неверно считанный баланс из файла!' unless @balance.is_a?(Integer)
    else
      @balance = DEFAULT_BALANCE
    end
  end

  # Получить текущий баланс
  attr_reader :balance

  # Сохранить баланс в файл
  def keep_balance
    File.write(PATH_BALANCE, @balance, mode: 'w')
  end

  # Оформить депозит на счет
  # @param [Integer] sum - сумма
  def deposit(sum)
    raise TypeError, 'Неверно введенная сумма' unless sum.is_a?(Integer)

    if !sum.positive?
      'Ошибка! Вы не можете ввести отрицательную сумму. Попробуйте еще раз.'
    else
      @balance += sum
      File.write(PATH_BALANCE, @balance, mode: 'w')
      "Теперь у вас #{@balance} вечнозеленых"
    end
  end

  # Оформить снятие со счета
  # @param [Integer] sum - сумма
  def withdraw(sum)
    raise TypeError, 'Неверно введенная сумма' unless sum.is_a?(Integer)

    if !sum.positive?
      'Невозможно вывести данную сумму. Попробуйте еще раз.'
    elsif sum > @balance
      'Недостаточно средств. Попробуйте еще раз.'
    else
      @balance -= sum
      File.write(PATH_BALANCE, @balance, mode: 'w')
      "Теперь у вас #{@balance} вечнозеленых"
    end
  end

end
