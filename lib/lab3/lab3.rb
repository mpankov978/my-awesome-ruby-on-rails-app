#Метод, принимающий число и слово, если слово заканчивается на "CS" - выводит на экран цифру 2 в степени длинны
# введенного слова ("macs" -> 16), если не заканчивается - выводит слово задом наперед ("ma" -> "am")
# @param [String] number - число
# @param [String] word - слово
def script1(number, word)
  raise TypeError, 'Метод принимает первый аргумент только как Integer, прочтите документацию!' unless (number.kind_of?(Integer))
  raise TypeError, 'Метод принимает второй аргумент только как String, прочтите документацию!' unless (word.kind_of?(String))
  word = word.chomp.to_s
  p word[-2..-1] == "cs" ? 2 ** word.size : word.reverse
end

#Метод, создающий и выводящий информацию о покемонах
def script2
  puts "Сколько покемонов добавить?"

  count = gets.to_i
  arr = []
  count.times do
    puts "Как назовем покемона?"

    name = gets.chomp.to_s
    puts "Какого цвета #{name}?"

    color = gets.chomp.to_s
    arr << { "name" => name, "color" => color }
  end
  p arr
end