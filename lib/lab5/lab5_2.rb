module Resource

  #Дает взаимодействовать на выбор с эндпоинтами GET/POST/PUT/DELETE запросов
  def connection(routes)
    if routes.nil?
      puts "No route matches for #{self}"
      return
    end

    loop do
      print 'Choose verb to interact with resources (GET/POST/PUT/DELETE) / q to exit: '
      verb = gets.chomp.strip.downcase
      break if verb == 'q'

      action = nil

      if verb == 'get'
        print 'Choose action (INDEX/SHOW) / q to exit: '
        action = gets.chomp.strip.downcase
        break if action == 'q'
      end
      action.nil? ? routes[verb].call : routes[verb][action].call
    end
  end
end

class PostsController
  extend Resource

  def initialize
    @posts = []
  end

  #Возвращает все посты из памяти и их индекс в массиве
  def index
    if @posts.size != 0
      @posts.each_with_index { |p, i|
        puts "#{i}. #{p}"
      }
    else
      puts "No posts yet."
    end
  end

  #Запрашивает идентификатор поста и показывает пост по переданному идентификатору
  def show
    puts "Input index of the post to show:"
    index = gets.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i - 1
    end
    puts "#{index}. #{@posts[index]}"
  end

  #Запрашивает текст поста, добавляет его в массив постов и возвращает в ответ идентификатор поста и сам пост
  def create
    puts "Input text of your new beautiful post:"
    post = gets.chomp.strip
    if post.empty?
      puts "Your post is empty. Is it not ready yet?"
    else
      @posts << post
      puts "#{@posts.size - 1}. #{@posts[-1]}"
    end
  end

  #Запрашивает идентификатор поста, потом новый текста поста и заменяет его. Возвращает в ответ обновленный пост
  def update
    puts "Input index of the post you want to update:"
    index = gets.chomp.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i
    end
    if @posts[index] == nil
      puts "Post not found!"
    else
      puts "Input new text of your post:"
      post = gets.chomp.strip
      @posts[index] = post
    end
  end

  #Запрашивает идентификатор поста, а затем удаляет его из массива постов
  def destroy
    puts "Input index of the post you want to delete:"
    index = gets.chomp.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i
    end
    @posts.delete_at(index)
  end
end

class CommentController
  extend Resource

  def initialize
    @comments = []
  end

  #Возвращает все комментарии из памяти и их индекс в массиве
  def index
    if @comments.size != 0
      @comments.each_with_index { |p, i|
        puts "#{i}. #{p}"
      }
    else
      puts "No comments yet."
    end
  end

  #Запрашивает идентификатор комментария и показывает комментарий по переданному идентификатору
  def show
    puts "Input index of the comment to show:"
    index = gets.chomp.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i
    end
    puts "#{index}. #{@comments[index]}"
  end

  #Запрашивает текст комментария, добавляет его в массив комментариев и возвращает в ответ идентификатор комментария
  # и сам комментарий
  def create
    puts "Input text of your new beautiful comment:"
    comment = gets.chomp.strip
    if comment.empty?
      puts "Your comment is empty. Is it not ready yet?"
    else
      @comments << comment
      puts "#{@comments.size - 1}. #{@comments[-1]}"
    end
  end

  #Запрашивает идентификатор комментария, потом новый текста комментария и заменяет его. Возвращает в ответ обновленный
  # комментарий
  def update
    puts "Write index of comment that you want to update."
    index = gets.chomp.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i
    end
    if @comments[index] == nil
      puts "Comment not found!"
    else
      puts "Input new text of your comment:"
      comment = gets.chomp.strip
      @comments[index] = comment
    end
  end

  #Запрашивает идентификатор комментария, а затем удаляет его из массива комментариев
  def destroy
    puts "Write index of comment that you want delete."
    index = gets.chomp.to_i
    while index < 0
      puts "Input number is < 0. Try again"
      index = gets.chomp.to_i
    end
    if @comments[index] == nil
      puts "Comment not found!"
    else
      @comments.delete_at(index)
    end
  end
end

#Осуществляет роутинг запросов на посты/комментарии
class Router
  def initialize
    @routes = {}
  end

  #Инициализация, предложение пользователю с чем взаимодействовать на выбор - с постами или комментариями
  def init
    resources(PostsController, 'posts')
    resources(CommentController, 'comments')
    loop do
      print 'Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): '
      choice = gets.chomp.strip

      PostsController.connection(@routes['posts']) if choice == '1'
      break if choice == 'q'

      CommentController.connection(@routes['comments']) if choice == '2'
      break if choice == 'q'
    end
    puts 'Good bye!'
  end

  #Маппинг HTTP запросов на ресурсы (методы-обработчики) контроллера
  def resources(klass, keyword)
    controller = klass.new
    @routes[keyword] = {
      'get' => {
        'index' => controller.method(:index),
        'show' => controller.method(:show)
      },
      'post' => controller.method(:create),
      'put' => controller.method(:update),
      'delete' => controller.method(:destroy)
    }
  end
end