NAMES_LIST_PATH = 'D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab4\names.txt'

#Считывает Фамилию Имя Возраст из файла, и записывает в result.txt те имена людей, чей возраст равен запрошенному
# из консоли
def show_names
  names_arr = []
  path_result = "results.txt"
  File.foreach(NAMES_LIST_PATH) { |line| names_arr << (line.chomp) }
  loop do
    if names_arr.size == 0
      puts "Все данные из файла считаны:\n"
      break
    end
    age = gets.to_i
    if age == -1
      puts "Получена команда -1:\n"
      break
    end
    names_arr.each { |s|
      File.write(path_result, "#{s}\n", mode: "a") if s.include?("#{age}")
      names_arr.delete(s) if s.include?("#{age}")
    }

  end
  File.foreach(path_result) { |line| puts line }
end
