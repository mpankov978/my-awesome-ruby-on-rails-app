PATH_BALANCE = 'D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab4\balance.txt'
DEFAULT_BALANCE = 100

#Осуществить депозит
# @param [Integer] balance - текущий баланс
# @return [Integer] balance - новый баланс
def deposit(balance)
  puts "Введите сумму: "
  sum = gets.to_i
  if sum < 0
    while sum < 0
      puts "Ошибка! Вы не можете ввести отрицательную сумму. Попробуйте еще раз."
      sum = gets.to_i
    end
  end
  balance = balance + sum
  File.write(PATH_BALANCE, "#{balance}", mode: "w")
  puts "Теперь у вас #{balance} вечнозеленых"
  balance
end

#Осуществить снятие денег
# @param [Integer] balance - текущий баланс
# @return [Integer] balance - новый баланс
def withdraw(balance)
  puts "Введите сумму: "
  draw = gets.to_i
  if draw < 0 or draw > balance
    while draw < 0 or draw > balance
      puts "Невозможно вывести данную сумму. Попробуйте еще раз."
      draw = gets.to_i
    end
  end
  balance = balance - draw
  File.write(PATH_BALANCE, balance, mode: "w")
  puts "Теперь у вас #{balance} вечнозеленых"
  balance
end

#Вывести баланс
# @param [Integer] balance - текущий баланс
def show_balance(balance)
  puts "Ваш баланс: #{balance} вечнозеленых"
end

#Логика банкомата
def atm_logic
  if File.exist?(PATH_BALANCE)
    balance = File.read(PATH_BALANCE).to_i
    raise TypeError, 'Неверно считанный баланс из файла!' unless (balance.kind_of?(Integer))
  else
    balance = DEFAULT_BALANCE
  end

  puts "Ваш баланс: #{balance} вечнозеленых"
  loop do
    action = gets
    case action
    when "q\n"
      File.write(PATH_BALANCE, balance, mode: "w")
      puts "Выход из программы"
      break
    when "b\n"
      show_balance(balance)
    when "w\n"
      balance = withdraw(balance)
    when "d\n"
      balance = deposit(balance)
    else
      puts "Не та команда!"
    end
  end
end
