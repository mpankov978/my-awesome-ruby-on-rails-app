ARTIST_LIST_PATH = 'D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab4\artist.txt'
BUFFER_FILE_PATH = 'D:\Универ\лабы\3к 5сем\Web-программирование\my-awesome-ruby-on-rails-app\lib\lab4\buffer.txt'

#Выводит все строки в файле
def index
  File.foreach(ARTIST_LIST_PATH) { |actor| puts actor }
end

#Находит конкретную строку в файле и выводит её
# @param [Integer] id - индекс
def find(id)
  raise TypeError, 'Метод принимает первый аргумент только как Integer, прочтите документацию!' unless (id.kind_of?(Integer))
  File.foreach(ARTIST_LIST_PATH).with_index do |actor, index|
    puts actor if id == index
  end
end

#Находит все строки в файле, где есть указанный паттерн
# @param [String] pattern - паттерн
def where(pattern)
  raise TypeError, 'Метод принимает первый аргумент только как String, прочтите документацию!' unless (pattern.kind_of?(String))
  File.foreach(ARTIST_LIST_PATH).with_index do |actor, index|
    @actor_index = index if actor.include?(pattern)
  end
  @actor_index
end

#Обновляет конкретную строку файла
# @param [Integer] id - индекс
# @param [String] text - новое значение
def update(id, text)
  raise TypeError, 'Метод принимает первый аргумент только как Integer, прочтите документацию!' unless (id.kind_of?(Integer))
  raise TypeError, 'Метод принимает второй аргумент только как String, прочтите документацию!' unless (text.kind_of?(String))
  file = File.open(BUFFER_FILE_PATH, "w")
  File.foreach(ARTIST_LIST_PATH).with_index do |actor, index|
    file.puts(id == index ? text : actor)
  end
  file.close
  File.write(ARTIST_LIST_PATH, File.read(BUFFER_FILE_PATH))
  File.delete(BUFFER_FILE_PATH) if File.exist?(BUFFER_FILE_PATH)
end

#Удаляет строку в файле
# @param [Integer] id - индекс
def delete(id)
  raise TypeError, 'Метод принимает первый аргумент только как Integer, прочтите документацию!' unless (id.kind_of?(Integer))
  file = File.open(BUFFER_FILE_PATH, "w")
  File.foreach(ARTIST_LIST_PATH).with_index do |actor, index|
    file.puts(actor) if index != id
  end
  file.close
  File.write(ARTIST_LIST_PATH, File.read(BUFFER_FILE_PATH))
  File.delete(BUFFER_FILE_PATH) if File.exist?(BUFFER_FILE_PATH)
end


