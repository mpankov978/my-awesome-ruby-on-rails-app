# Выводит два вида приветствия на экран в зависимости от введенного возраста
# @param [String] first_name - имя
# @param [String] second_name - фамилия
# @param [Integer] age - возраст
def print_greeting(first_name, second_name, age)
  raise TypeError, 'Метод принимает только Integer, прочтите документацию!' unless (age.kind_of?(Integer))
  if age < 18
    puts "Привет, #{first_name} #{second_name}. Тебе меньше 18 лет, но начать учиться программировать никогда не рано"
  else
    puts "Привет, #{first_name} #{second_name}. Самое время заняться делом!"
  end
end

# Метод, возвращающий число №2 если одно из них == 20,
# в противном случае возвращает их сумму
# @param [Integer] number_one - число
# @param [Integer] number_two - число
# @return [Integer] число
def foobar(number_one, number_two)
  raise TypeError, 'Метод принимает только Integer, прочтите документацию!' unless (number_one.kind_of?(Integer) && number_two.kind_of?(Integer))
  if number_one == 20 || number_two == 20
    number_two
  else
    number_one + number_two
  end
end
